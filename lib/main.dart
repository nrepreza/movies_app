import 'package:flutter/material.dart';
import 'package:movies_app/data/service_dependency/service_di.dart';
import 'package:movies_app/ui/movies_app.dart';

void main() async {
  await setupLocator();
  runApp(
    StateMovies(),
  );
}
