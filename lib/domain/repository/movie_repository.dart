import 'package:movies_app/domain/models/cast_model.dart';
import 'package:movies_app/domain/models/movie_model.dart';

abstract class MovieRepository {
  Future getMovies(int page);
  Future<List<Movie>> morePopular(int page);
  Future<List<Movie>> searchMovie(String query);
  Future<List<Cast>> fechCast(int movieId);
}
