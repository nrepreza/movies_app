import 'package:movies_app/domain/models/movie_model.dart';

abstract class FavoriteRepository {
  Future<List<Movie>> getFavoriteMovie();
  Future<bool> addFavoriteMovie(Movie movie);
  Future removedFavoriteMovie(Movie movie);
  Future<bool> isFavoriteMovie(Movie movie);
}
