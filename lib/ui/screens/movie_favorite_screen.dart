import 'package:flutter/material.dart';
import 'package:movies_app/domain/models/movie_model.dart';
import 'package:movies_app/providers/favorites_movies_provider.dart';
import 'package:movies_app/ui/utils/colors_util.dart';
import 'package:movies_app/ui/utils/enum_util.dart';
import 'package:movies_app/ui/utils/style_text_util.dart';
import 'package:movies_app/ui/widgets/bottom_navigate_widget.dart';
import 'package:movies_app/ui/widgets/cards_widget.dart';
import 'package:provider/provider.dart';

class MovieFavoriteScreen extends StatelessWidget {
  const MovieFavoriteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Movie> favoriteMovies = [];
    final movies = Provider.of<FavoriteMoviesProvider>(context).favoriteMovies;
    favoriteMovies = movies;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: backgroundDarkColor(),
        title: Text('Mis favoritas'),
        centerTitle: true,
      ),
      bottomNavigationBar: BottomMenu(
        pageSelected: PageSelected.Favorite,
      ),
      backgroundColor: backgroundDarkColor(),
      body: AnimatedSwitcher(
        duration: Duration(milliseconds: 500),
        child: favoriteMovies.isEmpty
            ? Container(
                alignment: Alignment.center,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.movie_creation_outlined, size: 50.0),
                    SizedBox(height: 10.0),
                    Text(
                      'No has agregado películas a tus favoritas.',
                      style: textStyle(
                        color: backgroundLiteColor(),
                        fontSize: 20.0,
                      ),
                    ),
                  ],
                ),
              )
            : ListView.builder(
                itemCount: favoriteMovies.length,
                itemBuilder: (_, index) {
                  return CardSearch(
                    movie: favoriteMovies[index],
                    movies: movies,
                    index: index,
                  );
                },
              ),
      ),
    );
  }
}
