import 'package:flutter/material.dart';
import 'package:movies_app/domain/models/movie_model.dart';
import 'package:movies_app/providers/movies_provider.dart';
import 'package:movies_app/ui/widgets/buttons_widget.dart';
import 'package:movies_app/ui/widgets/cards_widget.dart';
import 'package:provider/provider.dart';

class DetailsMovie extends StatefulWidget {
  final List<Movie> movies;
  final int index;
  const DetailsMovie({Key? key, required this.movies, required this.index})
      : super(key: key);

  @override
  _DetailsMovieState createState() => _DetailsMovieState();
}

class _DetailsMovieState extends State<DetailsMovie> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Stack(
          children: [
            Container(
              child: PageView.builder(
                  onPageChanged: (page) {
                    if (page == widget.movies.length - 3) {
                      Provider.of<MovieProvider>(context, listen: false)
                          .getMovieProvider();
                      print(page);
                    }
                  },
                  scrollDirection: Axis.vertical,
                  controller: PageController(initialPage: widget.index),
                  itemCount: widget.movies.length,
                  itemBuilder: (context, index) {
                    return CardDetail(
                      movie: widget.movies[index],
                    );
                  }),
            ),
            Positioned(
              top: 15.0,
              left: 20.0,
              child: GoToBack(),
            ),
          ],
        ),
      ),
    );
  }
}
