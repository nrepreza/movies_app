import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:movies_app/ui/utils/colors_util.dart';
import 'package:movies_app/ui/utils/navigations_util.dart';
import 'package:movies_app/ui/utils/style_text_util.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  final dur = Tween(end: 1.2, begin: 0.0);
  //Funcion
  //navegar a la pantalla principal luego de 3 segundos...
  goToHome() async {
    await Future.delayed(Duration(seconds: 2), () => navigateToHome(context));
  }

  @override
  void initState() {
    goToHome();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitDown,
      DeviceOrientation.portraitUp,
    ]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        color: Colors.black,
        alignment: Alignment.center,
        child: TweenAnimationBuilder(
          curve: Curves.easeIn,
          tween: dur,
          duration: Duration(milliseconds: 1000),
          builder: (_, double scalaa, widget) {
            return Transform.scale(
              scale: scalaa,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Movies App',
                    style: textStyle(
                      color: backgroundLiteColor(),
                      fontSize: 40.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(width: 10.0),
                  Icon(
                    Icons.movie_creation_outlined,
                    size: 55.0,
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
