import 'package:flutter/material.dart';
import 'package:movies_app/providers/movies_provider.dart';
import 'package:movies_app/ui/utils/colors_util.dart';
import 'package:movies_app/ui/utils/enum_util.dart';
import 'package:movies_app/ui/utils/navigations_util.dart';
import 'package:movies_app/ui/utils/search_util.dart';
import 'package:movies_app/ui/utils/style_text_util.dart';
import 'package:movies_app/ui/widgets/bottom_navigate_widget.dart';
import 'package:movies_app/ui/widgets/cards_widget.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final ScrollController scrollController = new ScrollController();
  double pixel = 0.0;
  double maxPixel = 0.0;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final _moviesProvider = Provider.of<MovieProvider>(context, listen: false);
    if (_moviesProvider.movies.length > 0) {
      if (maxPixel == pixel - 500) return _moviesProvider.getMorePopular();
      return Scaffold(
        appBar: AppBar(
          leading: Container(
            margin: EdgeInsets.only(left: 20.0),
            alignment: Alignment.centerLeft,
            height: double.maxFinite,
            width: size.width * 0.8,
            child: Text(
              'Bienvenido!',
              style: textStyle(
                color: primaryColor(),
                fontSize: 20.0,
              ),
            ),
          ),
          leadingWidth: size.width * 0.7,
          backgroundColor: Colors.black,
          automaticallyImplyLeading: false,
          actions: [
            IconButton(
              onPressed: () {
                showSearch(context: context, delegate: MovieSearch());
              },
              icon: Icon(
                Icons.search,
                size: 30.0,
                color: primaryColor(),
              ),
            )
          ],
        ),
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Transform.scale(
            scale: 1,
            child: Container(
              color: Colors.black,
              margin: EdgeInsets.symmetric(vertical: 10.0),
              alignment: Alignment.center,
              child: AnimatedSwitcher(
                duration: Duration(milliseconds: 600),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20.0),
                        child: Text(
                          'Conoce las más recientes',
                          style: textStyle(
                            color: backgroundLiteColor(),
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        // reverse: true,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: List.generate(
                            _moviesProvider.movies.length,
                            (index) {
                              return PrincipalCard(
                                index: index,
                                movies: _moviesProvider.movies,
                                movie: _moviesProvider.movies[index],
                                onTap: () => navigationToDetailMovie(
                                  context,
                                  movies: _moviesProvider.movies,
                                  index: index,
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: 20.0,
                          vertical: 10.0,
                        ),
                        child: Text(
                          'Más vistas',
                          style: textStyle(
                            color: backgroundLiteColor(),
                            fontSize: 30.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SingleChildScrollView(
                        controller: scrollController,
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: List.generate(
                            _moviesProvider.morePopular.length,
                            (index) {
                              final movie = _moviesProvider.morePopular[index];
                              return SecundaryCard(
                                index: index,
                                movie: movie,
                                movies: _moviesProvider.morePopular,
                                title: _moviesProvider.morePopular[index].title,
                                urlImage: movie.fullUrlImage,
                                description:
                                    _moviesProvider.morePopular[index].overview,
                              );
                            },
                          ),
                        ),
                      ),
                      SizedBox(height: 10.0),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        bottomNavigationBar: BottomMenu(
          pageSelected: PageSelected.Home,
        ),
      );
    }
    return Container(
      width: 30.0,
      height: 30.0,
      child: CircularProgressIndicator(),
    );
  }
}
