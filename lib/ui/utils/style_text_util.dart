import 'package:flutter/painting.dart';
import 'package:google_fonts/google_fonts.dart';

TextStyle textStyle(
    {required Color color,
    double fontSize = 15.0,
    FontWeight fontWeight = FontWeight.normal}) {
  return GoogleFonts.lato(
    textStyle: TextStyle(
      color: color,
      fontSize: fontSize,
      fontWeight: fontWeight,
    ),
  );
}
