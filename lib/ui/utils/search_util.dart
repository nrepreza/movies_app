import 'package:flutter/material.dart';
import 'package:movies_app/domain/models/movie_model.dart';
import 'package:movies_app/providers/movies_provider.dart';
import 'package:movies_app/ui/utils/colors_util.dart';
import 'package:movies_app/ui/utils/style_text_util.dart';
import 'package:movies_app/ui/widgets/cards_widget.dart';
import 'package:provider/provider.dart';

class MovieSearch extends SearchDelegate {
  List<Movie> moviesShearch = [];
  @override
  String? get searchFieldLabel => 'Buscar películas';

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () {
          query = '';
          moviesShearch = [];
        },
        icon: Icon(Icons.clear),
      ),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      onPressed: () => close(context, null),
      icon: Icon(Icons.arrow_back),
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    if (moviesShearch.length == 0)
      return Container(
        alignment: Alignment.center,
        child: SizedBox(
          height: 90.0,
          child: Column(
            children: [
              Icon(
                Icons.search,
                size: 50.0,
              ),
              Text(
                'No se encontro una película con ese nombre',
                style: textStyle(
                  color: backgroundLiteColor(),
                  fontSize: 20.0,
                ),
              ),
            ],
          ),
        ),
      );
    return ListView.builder(
        itemCount: moviesShearch.length,
        itemBuilder: (_, index) {
          return CardSearch(
              movie: moviesShearch[index], movies: moviesShearch, index: index);
        });
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final _moviesProvider = Provider.of<MovieProvider>(context, listen: false);
    if (query.isEmpty) {
      return Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.search,
              size: 40.0,
            ),
            SizedBox(height: 10.0),
            Text(
              'Intenta buscando cualquier película',
            ),
          ],
        ),
      );
    }
    _moviesProvider.getsuggProvider(query);
    return StreamBuilder(
        stream: _moviesProvider.suggStream,
        builder: (_, AsyncSnapshot<List<Movie>> stream) {
          if (!stream.hasData) return _emptyWidget();
          final movies = stream.data!;
          moviesShearch = stream.data!;
          return ListView.builder(
            itemCount: movies.length,
            itemBuilder: (_, index) {
              return CardSearch(
                movie: movies[index],
                index: index,
                movies: movies,
              );
            },
          );
        });
  }
}

Widget _emptyWidget() => Container(
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.search,
            size: 40.0,
          ),
          SizedBox(height: 10.0),
          Text(
            'Intenta buscando cualquier películas',
          ),
        ],
      ),
    );
