import 'package:flutter/material.dart';

//backgrounds colors
Color primaryColor() => Color(0xFFc77dff);
Color secondaryColor() => Color(0xFF10002b);
Color backgroundLiteColor() => Color(0xFFFFFFFF);
Color backgroundDarkColor() => Color(0xFF000000);
