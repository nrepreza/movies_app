import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:movies_app/domain/models/movie_model.dart';
import 'package:movies_app/ui/screens/detail_movie_screen.dart';
import 'package:movies_app/ui/screens/home_screen.dart';
import 'package:movies_app/ui/screens/movie_favorite_screen.dart';

//Navegacion hacia la pantalla principal...
navigateToHome(BuildContext context) => Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(builder: (context) => HomeScreen()),
      (route) => false,
    );

//Navegacion hacia la pantalla de detalle de pelicula...
navigationToDetailMovie(BuildContext context,
        {required List<Movie> movies, required int index}) =>
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => DetailsMovie(
          movies: movies,
          index: index,
        ),
      ),
    );

navigationToFavoriteScreen(BuildContext context) => Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MovieFavoriteScreen(),
      ),
    );
