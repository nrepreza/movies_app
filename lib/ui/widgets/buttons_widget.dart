import 'package:flutter/material.dart';
import 'package:movies_app/ui/utils/colors_util.dart';

class PrimaryButton extends StatelessWidget {
  final bool isliked;
  final String title;
  const PrimaryButton({Key? key, this.title = 'title', this.isliked = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      // width: MediaQuery.of(context).size.width * 0.6,
      padding: EdgeInsets.symmetric(
        horizontal: 20.0,
        vertical: 10.0,
      ),
      margin: EdgeInsets.only(bottom: 24.0),
      width: MediaQuery.of(context).size.width * 0.5,
      height: MediaQuery.of(context).size.height * 0.08,

      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: isliked
              ? [
                  primaryColor(),
                  secondaryColor(),
                ]
              : [
                  primaryColor(),
                  primaryColor(),
                ],
        ),
        borderRadius: BorderRadius.circular(30.0),
        // color: Colors.red,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            title,
            style: Theme.of(context).textTheme.headline2,
          ),
          SizedBox(width: 10.0),
          Icon(
            Icons.favorite,
            color: isliked ? primaryColor() : Colors.white,
          )
        ],
      ),
    );
  }
}

class GoToBack extends StatelessWidget {
  const GoToBack({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.pop(context),
      child: Container(
        width: MediaQuery.of(context).size.width * 0.12,
        height: MediaQuery.of(context).size.width * 0.07,
        child: Icon(
          Icons.arrow_back,
        ),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          boxShadow: [
            BoxShadow(
              color: Color(0xFFc77dff).withOpacity(0.6),
              spreadRadius: 1.0,
              blurRadius: 3.0,
            )
          ],
          gradient: LinearGradient(
            colors: [
              primaryColor(),
              secondaryColor(),
            ],
          ),
        ),
      ),
    );
  }
}
