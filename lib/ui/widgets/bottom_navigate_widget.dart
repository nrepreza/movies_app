import 'package:flutter/material.dart';
import 'package:movies_app/ui/utils/colors_util.dart';
import 'package:movies_app/ui/utils/enum_util.dart';
import 'package:movies_app/ui/utils/navigations_util.dart';

class BottomMenu extends StatelessWidget {
  final PageSelected pageSelected;
  const BottomMenu({Key? key, this.pageSelected = PageSelected.Home})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Container(
      alignment: Alignment.center,
      width: size.width,
      height: size.height * 0.08,
      color: Colors.black,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          IconButton(
            onPressed: () => navigateToHome(context),
            icon: Icon(
              Icons.home_filled,
              size: 30.0,
              color: pageSelected == PageSelected.Home ? primaryColor() : null,
            ),
          ),
          IconButton(
            onPressed: () => navigationToFavoriteScreen(context),
            icon: Icon(
              Icons.favorite,
              color:
                  pageSelected == PageSelected.Favorite ? primaryColor() : null,
              size: 30.0,
            ),
          )
        ],
      ),
    );
  }
}
