import 'package:flutter/material.dart';
// import 'package:movies_app/domain/models/movie_model.dart';
import 'package:movies_app/ui/utils/colors_util.dart';
import 'package:movies_app/ui/utils/style_text_util.dart';

class MovieItem extends StatelessWidget {
  final String urlImage;
  final String nombre;
  // final Movie movie;
  const MovieItem(
      {Key? key,
      // required this.movie,
      required this.urlImage,
      this.nombre = 'title'})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.19,
      // height: size.width * 0.4,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.only(
              left: 15.0,
              right: 5.0,
              // top: 10.0,
            ),
            width: size.width * 0.19,
            height: size.width * 0.15,
            decoration: BoxDecoration(
              color: primaryColor(),
              borderRadius: BorderRadius.circular((size.width * 0.2) / 2),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular((size.width * 0.2) / 2),
              child: Image.network(
                urlImage,
                fit: BoxFit.cover,
              ),
            ),
          ),
          SizedBox(height: 10.0),
          Text(
            nombre,
            style: textStyle(
              color: backgroundLiteColor(),
              fontSize: 10.0,
            ),
            textAlign: TextAlign.center,
            maxLines: 2,
          ),
        ],
      ),
    );
  }
}
