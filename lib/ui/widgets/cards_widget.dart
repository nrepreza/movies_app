import 'package:flutter/material.dart';
import 'package:movies_app/domain/models/cast_model.dart';
import 'package:movies_app/domain/models/movie_model.dart';
import 'package:movies_app/providers/favorites_movies_provider.dart';
import 'package:movies_app/providers/movies_provider.dart';
import 'package:movies_app/ui/utils/colors_util.dart';
import 'package:movies_app/ui/utils/const_util.dart';
import 'package:movies_app/ui/utils/navigations_util.dart';
import 'package:movies_app/ui/utils/style_text_util.dart';
import 'package:movies_app/ui/widgets/movie_item_widget.dart';
import 'package:provider/provider.dart';

class PrincipalCard extends StatefulWidget {
  final Movie movie;
  final Function onTap;
  final int index;
  final List<Movie> movies;
  const PrincipalCard({
    Key? key,
    required this.onTap,
    required this.movie,
    required this.movies,
    required this.index,
  }) : super(key: key);

  @override
  _PrincipalCardState createState() => _PrincipalCardState();
}

class _PrincipalCardState extends State<PrincipalCard> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Stack(
          children: [
            GestureDetector(
              onTap: () => navigationToDetailMovie(context,
                  movies: widget.movies, index: widget.index),
              child: Container(
                margin: EdgeInsets.only(top: 10.0, left: 20.0, right: 5),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: FadeInImage(
                    placeholder: AssetImage(LOANDING_GIF),
                    fit: BoxFit.cover,
                    image: NetworkImage(
                      widget.movie.fullUrlImage,
                    ),
                  ),
                ),
                width: size.width * 0.35,
                height: size.height * 0.30,
                decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  borderRadius: BorderRadius.circular(20.0),
                  boxShadow: [
                    BoxShadow(
                      color: primaryColor(),
                      spreadRadius: 0.5,
                      blurRadius: 5.0,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        SizedBox(height: 10.0),
        Container(
          width: size.width * 0.35,
          margin: EdgeInsets.only(left: 20.0),
          child: Text(
            widget.movie.title,
            style: textStyle(
              color: backgroundLiteColor(),
              fontSize: 15.0,
              fontWeight: FontWeight.bold,
            ),
            maxLines: 2,
          ),
        ),
      ],
    );
  }
}

class SecundaryCard extends StatelessWidget {
  final String title;
  final String urlImage;
  final String description;
  final Movie movie;
  final List<Movie> movies;
  final int index;
  const SecundaryCard({
    Key? key,
    this.title = 'Name',
    this.urlImage = '',
    this.description = 'data',
    required this.movie,
    required this.movies,
    required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        GestureDetector(
          onTap: () =>
              navigationToDetailMovie(context, movies: movies, index: index),
          child: Container(
            margin: EdgeInsets.only(top: 10.0, left: 20.0, right: 5),
            child: Hero(
              tag: '${movie.id}',
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20.0),
                child: FadeInImage(
                  placeholder: AssetImage(LOANDING_GIF),
                  fit: BoxFit.cover,
                  image: NetworkImage(
                    movie.fullUrlImage,
                  ),
                ),
              ),
            ),
            width: size.width * 0.26,
            height: size.height * 0.26,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              boxShadow: [
                BoxShadow(
                  color: primaryColor(),
                  spreadRadius: 1.0,
                  blurRadius: 3.0,
                ),
              ],
            ),
          ),
        ),
        SizedBox(width: 5.0),
        GestureDetector(
          onTap: () =>
              navigationToDetailMovie(context, movies: movies, index: index),
          child: Container(
            width: size.width * 0.4,
            margin: EdgeInsets.only(left: 10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  movie.title,
                  style: TextStyle(
                    color: primaryColor(),
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                  ),
                  maxLines: 2,
                ),
                SizedBox(height: 5.0),
                Text(
                  this.description == ''
                      ? 'Esta pelicula no tiene una descripcion precargada'
                      : this.description,
                  style: textStyle(
                    color: backgroundLiteColor(),
                    fontSize: 15.0,
                  ),
                  maxLines: 8,
                ),
                SizedBox(height: 5.0),
                Row(
                  children: [
                    Icon(
                      Icons.favorite,
                      size: 25.0,
                      color: primaryColor(),
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      '${movie.popularity} ',
                      style: TextStyle(
                        color: primaryColor(),
                        fontSize: 14.0,
                      ),
                    ),
                    SizedBox(width: 5),
                  ],
                ),
                SizedBox(height: 15.0),
              ],
            ),
          ),
        ),
      ],
    );
  }
}

class CardDetail extends StatelessWidget {
  final Movie movie;
  final bool isLiked;

  const CardDetail({Key? key, required this.movie, this.isLiked = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final favoriteProvider = Provider.of<FavoriteMoviesProvider>(context);
    final castProvider = Provider.of<MovieProvider>(context);
    // List<Movie> favoriteMovies = [];
    // bool liked = isLiked;
    return Container(
      width: size.width,
      height: size.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Stack(
            clipBehavior: Clip.none,
            children: [
              Hero(
                tag: '${movie.id}',
                child: Image.network(
                  movie.fullUrlImage,
                  width: size.width,
                  height: size.height * 0.6,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                alignment: Alignment.bottomCenter,
                width: size.width,
                height: size.height * 0.6,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [
                      Colors.transparent,
                      Colors.black,
                    ],
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    stops: [
                      0.6,
                      1.0,
                    ],
                  ),
                ),
              ),
              Positioned(
                top: 05.0,
                right: 0.0,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 5.0),
                  child: GestureDetector(
                    onTap: () => favoriteProvider.saveFavoriteMovie(movie),
                    child: Icon(
                      Icons.favorite,
                      color: isLiked ? primaryColor() : backgroundLiteColor(),
                      size: 30.0,
                    ),
                  ),
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Text(
              movie.title,
              style: textStyle(
                color: primaryColor(),
                fontSize: 22.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          SizedBox(height: 10.0),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Text(
              movie.overview == ''
                  ? 'Esta pelicula no tiene una descripcion precargada'
                  : movie.overview,
              maxLines: 4,
              textAlign: TextAlign.justify,
            ),
          ),
          SizedBox(height: 10.0),
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                'Elenco',
                style: TextStyle(
                  color: primaryColor(),
                  fontSize: 20.0,
                ),
                textAlign: TextAlign.left,
              ),
            ),
          ),
          FutureBuilder(
            future: castProvider.fetchCast(movie.id),
            builder: (_, AsyncSnapshot<List<Cast>> snapshot) {
              if (!snapshot.hasData) {
                return Container(
                  width: 20.0,
                  height: 20.0,
                  child: CircularProgressIndicator(),
                );
              }
              final List<Cast> cast = snapshot.data!;
              // if(cast =)
              return Row(
                mainAxisAlignment: cast.length > 4
                    ? MainAxisAlignment.spaceBetween
                    : MainAxisAlignment.start,
                children: cast.length == 0
                    ? [
                        Container(
                          width: size.width,
                          padding: EdgeInsets.symmetric(horizontal: 20.0),
                          child: Text(
                            'Esta pelicula no tiene un elenco precargado',
                            style: textStyle(
                              color: backgroundLiteColor(),
                              fontSize: 15.0,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        )
                      ]
                    : List.generate(
                        cast.length > 4 ? 4 : cast.length,
                        (index) => Container(
                          margin: EdgeInsets.only(
                            left: 10.0,
                            right: 5.0,
                            bottom: 10.0,
                            top: 5.0,
                          ),
                          child: MovieItem(
                            urlImage: cast[index].fullUrlImage,
                            nombre: cast[index].name!,
                          ),
                        ),
                      ),
              );
            },
          ),
        ],
      ),
    );
  }
}

class CardSearch extends StatelessWidget {
  final Movie movie;
  final List<Movie> movies;
  final int index;
  const CardSearch(
      {Key? key,
      required this.movie,
      required this.movies,
      required this.index})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () =>
          navigationToDetailMovie(context, movies: movies, index: index),
      child: Container(
        alignment: Alignment.center,
        margin:
            EdgeInsets.only(left: 20.0, right: 20.0, top: 20.0, bottom: 10.0),
        width: size.width,
        height: size.height * 0.23,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Flexible(
              flex: 3,
              child: Container(
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: primaryColor().withOpacity(0.6),
                      spreadRadius: 1,
                      blurRadius: 2,
                    )
                  ],
                  borderRadius: BorderRadius.circular(20.0),
                ),
                width: double.maxFinite,
                height: double.maxFinite,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20.0),
                  child: Hero(
                    tag: '${movie.id}',
                    child: Image.network(
                      movie.fullUrlImage,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ),
            Flexible(
              flex: 7,
              child: Container(
                margin: EdgeInsets.only(
                  left: 10.0,
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      movie.title,
                      style: textStyle(
                        color: primaryColor(),
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 10.0),
                    Text(
                      movie.overview == ''
                          ? 'Pelicula no posee una descripcion precargada'
                          : movie.overview,
                      maxLines: 6,
                    ),
                    SizedBox(height: 10.0),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
