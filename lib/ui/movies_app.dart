import 'package:flutter/material.dart';
import 'package:movies_app/providers/favorites_movies_provider.dart';
import 'package:movies_app/providers/movies_provider.dart';
import 'package:movies_app/ui/screens/splash_screen.dart';
import 'package:provider/provider.dart';

class StateMovies extends StatelessWidget {
  const StateMovies({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => MovieProvider(),
          lazy: false,
        ),
        ChangeNotifierProvider(
          create: (_) => FavoriteMoviesProvider(),
          lazy: false,
        ),
      ],
      child: MoviesApp(),
    );
  }
}

class MoviesApp extends StatelessWidget {
  // This widget is the root of your application.

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Movies App',
      theme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.blue,
        accentColor: Colors.blueAccent[800],
        textTheme: const TextTheme(
          headline1: TextStyle(
            color: Colors.white,
            fontSize: 30.0,
            fontWeight: FontWeight.bold,
          ),
          headline2: TextStyle(
            color: Colors.white,
            fontSize: 20.0,
            fontWeight: FontWeight.w500,
          ),
          headline4: TextStyle(
            color: Colors.white,
            fontSize: 15.0,
            fontWeight: FontWeight.w500,
          ),
          headline5: TextStyle(
            color: Colors.white,
            fontSize: 10.0,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      home: SplashScreen(),
    );
  }
}
