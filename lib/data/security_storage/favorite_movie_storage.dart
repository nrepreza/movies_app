import 'dart:convert';

import 'package:movies_app/domain/models/movie_model.dart';
import 'package:movies_app/domain/repository/favorite_repository.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

class FavoriteMovieStorage implements FavoriteRepository {
  final _storage = FlutterSecureStorage();
  List<Movie> _movies = [];

  @override
  Future<bool> addFavoriteMovie(Movie movie) async {
    List<Movie> _contain = [];
    List<Movie> _saveMoviesFavorite = await getFavoriteMovie();
    if (_saveMoviesFavorite.length > 0) {
      _contain = _saveMoviesFavorite;
      List<Movie> _moviesEquals =
          _contain.where((element) => element.id == movie.id).toList();
      if (_moviesEquals.length > 0) {
        _saveMoviesFavorite.removeWhere((element) => element.id == movie.id);
        _movies = _saveMoviesFavorite;
        await _storage.write(key: 'favoriteMovies', value: _movies.toString());
      } else {
        _contain.add(movie);
        await _storage.write(key: 'favoriteMovies', value: _movies.toString());
        return true;
      }
      return true;
    }

    _movies.add(movie);
    await _storage.write(key: 'favoriteMovies', value: _movies.toString());
    return true;
  }

  @override
  Future<List<Movie>> getFavoriteMovie() async {
    try {
      var data = await _storage.read(key: 'favoriteMovies');
      _movies = jsonDecode(data!);
    } catch (e) {}
    return _movies;
  }

  @override
  Future removedFavoriteMovie(Movie movie) {
    throw UnimplementedError();
  }

  @override
  Future<bool> isFavoriteMovie(Movie movie) async {
    var res = await getFavoriteMovie();
    _movies = res.where((element) => element.id == movie.id).toList();
    if (_movies.length > 0) {
      return true;
    }
    return false;
  }
}
