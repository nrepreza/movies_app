import 'package:movies_app/data/core/constans_core.dart';
import 'package:http/http.dart' as http;

Future<String> callHeaderHttp(String endpoint, [int page = 1]) async {
  var url = Uri.https(
    BASE_URL,
    endpoint,
    {
      'language': LANGUAGE,
      'api_key': API_KEY,
      'page': page,
    },
  );
  var response = await http.get(url);
  return response.body;
}
