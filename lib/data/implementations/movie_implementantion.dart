import 'package:movies_app/data/core/constans_core.dart';
import 'package:movies_app/domain/models/cast_model.dart';
import 'package:movies_app/domain/models/movie_model.dart';
import 'package:movies_app/domain/models/now_playing_model.dart';
import 'package:movies_app/domain/models/popular_movie_model.dart';
import 'package:movies_app/domain/models/search_movie_response_model.dart';
import 'package:movies_app/domain/repository/movie_repository.dart';
import 'package:http/http.dart' as http;

class MovieImplementation implements MovieRepository {
  String _endpointUrl = '3/movie/now_playing';
  String _popularEndpoint = '3/movie/popular';
  String _endpointSearch = '3/search/movie';

  List<Movie> _movies = [];
  List<Movie> _resultSearch = [];
  List<Cast> _cast = [];

  //metodo que devuelve las peliculas
  @override
  Future getMovies(int page) async {
    try {
      var url = Uri.https(
        BASE_URL,
        _endpointUrl,
        {
          'language': LANGUAGE,
          'api_key': API_KEY,
          'page': '$page',
        },
      );
      var response = await http.get(url);
      if (response.statusCode == 200) {
        final res = NowPlayingResponse.fromJson(response.body);
        _movies = res.results;
        return _movies;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }

  //devuelve las mas populares...
  @override
  Future<List<Movie>> morePopular(int page) async {
    try {
      var url = Uri.https(
        BASE_URL,
        _popularEndpoint,
        {
          'language': LANGUAGE,
          'api_key': API_KEY,
          'page': '$page',
        },
      );
      var response = await http.get(url);
      if (response.statusCode == 200) {
        final res = PopularResponse.fromJson(response.body);
        _movies = res.results;
      } else {
        return [];
      }
    } catch (error) {
      print(error);
    }
    return _movies;
  }

  @override
  Future<List<Movie>> searchMovie(String query) async {
    try {
      var url = Uri.https(
        BASE_URL,
        _endpointSearch,
        {
          'language': LANGUAGE,
          'api_key': API_KEY,
          'page': '2',
          'query': '$query',
        },
      );

      var response = await http.get(url);

      final res = SearchMovieResponse.fromJson(response.body);
      _resultSearch = res.results;
    } catch (error) {
      print(error);
    }
    return _resultSearch;
  }

  @override
  Future<List<Cast>> fechCast(int movieId) async {
    try {
      var url = Uri.https(
        BASE_URL,
        '3/movie/$movieId/credits',
        {
          'language': LANGUAGE,
          'api_key': API_KEY,
          'page': '2',
        },
      );
      var res = await http.get(url);
      final castResponse = CreditsResponse.fromJson(res.body);
      _cast = castResponse.cast;
      // print(_cast);
    } catch (e) {
      print(e);
    }
    return _cast;
  }
}
