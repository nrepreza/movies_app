import 'package:get_it/get_it.dart';
import 'package:movies_app/data/implementations/movie_implementantion.dart';
import 'package:movies_app/data/security_storage/favorite_movie_storage.dart';
import 'package:movies_app/domain/repository/favorite_repository.dart';
import 'package:movies_app/domain/repository/movie_repository.dart';

//llamar a esta instancia para obtener la implementacion por me de la clase abstracta
GetIt serviceLocator = GetIt.instance;

Future<void> setupLocator() async {
  // inyeccion de dependecia de peliculas
  serviceLocator.registerLazySingleton<MovieRepository>(
    () => MovieImplementation(),
  );

  serviceLocator.registerLazySingleton<FavoriteRepository>(
    () => FavoriteMovieStorage(),
  );
}
