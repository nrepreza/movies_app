import 'package:flutter/material.dart';
import 'package:movies_app/data/service_dependency/service_di.dart';
import 'package:movies_app/domain/models/movie_model.dart';
import 'package:movies_app/domain/repository/favorite_repository.dart';

class FavoriteMoviesProvider extends ChangeNotifier {
  final serviceFavoritesMovies = serviceLocator<FavoriteRepository>();
  List<Movie> favoriteMovies = [];
  bool isFavorite = false;
  FavoriteMoviesProvider() {
    this.getFavoriteMovie();
  }

  getFavoriteMovie() async {
    List<Movie> movies = await serviceFavoritesMovies.getFavoriteMovie();
    favoriteMovies = movies;
    notifyListeners();
  }

  saveFavoriteMovie(Movie movie) async {
    var response = await serviceFavoritesMovies.addFavoriteMovie(movie);
    isFavorite = response;
    notifyListeners();
  }

  Future<bool> isFavoriteMovie(Movie movie) async {
    bool response = await serviceFavoritesMovies.isFavoriteMovie(movie);
    return response;
  }
}
