import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:movies_app/data/service_dependency/service_di.dart';
import 'package:movies_app/domain/models/cast_model.dart';
import 'package:movies_app/domain/models/movie_model.dart';
import 'package:movies_app/domain/repository/movie_repository.dart';
import 'package:movies_app/ui/utils/debounce_util.dart';

class MovieProvider extends ChangeNotifier {
  List<Movie> movies = [];
  List<Movie> morePopular = [];
  List<Movie> moreRecent = [];
  List<Movie> results = [];
  Map<int, List<Cast>> movieCast = {};
  int page = 1;
  List<Movie> moreMovies = [];

  final debouncer = Debouncer(duration: Duration(milliseconds: 500));

  final StreamController<List<Movie>> _sugStrmController =
      new StreamController.broadcast();
  Stream<List<Movie>> get suggStream => this._sugStrmController.stream;

  MovieProvider() {
    this.getMovieProvider();
    this.getMorePopular();
  }

  final serviceMovies = serviceLocator<MovieRepository>();
  getMovieProvider() async {
    page++;
    var results = await serviceMovies.getMovies(page);
    results.forEach((movie) {
      moreMovies.add(movie);
    });
    movies = results;
    notifyListeners();
  }

  getMorePopular() async {
    page++;
    var results = await serviceMovies.morePopular(page);
    results.forEach((element) {
      morePopular.add(element);
    });
    notifyListeners();
  }

  Future<List<Movie>> searchMovies(String query) async {
    var res = await serviceMovies.searchMovie(query);
    results = res;
    return results;
    // notifyListeners();
  }

  Future<List<Cast>> fetchCast(int movieId) async {
    if (movieCast.containsKey(movieId)) return movieCast[movieId]!;
    List<Cast> casts = await serviceMovies.fechCast(movieId);
    movieCast[movieId] = casts;
    return casts;
  }

  void getsuggProvider(String searchTerm) {
    debouncer.value = '';
    debouncer.onValue = (value) async {
      // print('data?');
      final results = await this.searchMovies(value);
      this._sugStrmController.add(results);
    };

    final _timer = Timer.periodic(Duration(milliseconds: 300), (_) {
      debouncer.value = searchTerm;
    });

    Future.delayed(Duration(milliseconds: 301))
        .then((value) => _timer.cancel());
  }
}
